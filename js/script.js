const apiUrl = "https://www.ameliarueda.com//endpoints/stories";
let isClicked = false;
let isMoveLeft = null;
let modalIsOpen = false;
let mousePressTime = {};

const loadStories = type => {
  const lastStorie = sessionStorage.getItem("state");
  const currentStorie = Number(sessionStorage.getItem("stories#"));
  if (lastStorie === type) {
    sessionStorage.setItem(
      "stories#",
      currentStorie === 0 ? currentStorie : currentStorie - 1
    );
  } else {
    sessionStorage.setItem("stories#", 0);
  }
  sessionStorage.setItem("state", type);
  loadStorieByType(type);
  shoModalStories(true);
};

loadStorieByType = type => {
  switch (type) {
    case "gettingMemes":
      getMemes(type);
      break;

    case "gettingInfographics":
      getInfographics(type);
      break;

    case "gettingVideos":
      getVideos(type);
      break;

    case "gettingPodcasts":
      getPodcasts(type);
      break;

    default:
      break;
  }
};

const readStories = lastStorie => {
  for (let index = 0; index < lastStorie - 1; index++) {
    document.getElementById(`progressBarDiv${index}`).style.width = `100%`;
  }
};

// show and hide modal
const shoModalStories = isShow => {
  const modal = document.getElementById("modalStories");
  modal.style.display = `${isShow ? "block" : "none"}`;
  modalIsOpen = isShow;
};

// get data from endpoint
const getVideos = (type) => {
  $.ajax({
    url: apiUrl,
    type: "GET",
    success: function(data) {
      renderStories(data.videos, type);
    },
    error: function() {
      alert("Api Error");
    }
  });
};
const getMemes = (type) => {
  $.ajax({
    url: apiUrl,
    type: "GET",
    dataType: "json",
    success: function(data) {
      renderStories(data.memes, type);
    },
    error: function() {
      alert("Api Error");
    }
  });
};
const getInfographics = (type) => {
  $.ajax({
    url: apiUrl,
    type: "GET",
    success: function(data) {
      renderStories(data.infografias, type);
    },
    error: function() {
      alert("Api Error");
    }
  });
};
const getPodcasts = (type) => {
  $.ajax({
    url: apiUrl,
    type: "GET",
    success: function(data) {
      renderStories(data.podcasts, type);
    },
    error: function() {
      alert("Api Error");
    }
  });
};

renderStories = (obj, type) => {
  document.getElementById("ctnStoriesBody").innerHTML = "";
  document.getElementById("ctnStoriesHeader").innerHTML = "";
  obj.map((value, index) => {
    renderHeader(Object.keys(obj).length, index);
    switch (type) {
      case "gettingMemes":
        renderStorieCtn(index, value.asset_url, Object.keys(obj).length, true);
        break;

      case "gettingInfographics":
        renderStorieCtn(index, value.asset_url, Object.keys(obj).length, true);
        break;

      case "gettingVideos":
        renderStorieCtn(index, value.asset_url, Object.keys(obj).length, false);
        break;

      default:
        break;
    }
  });
  //load the initial storie
  loadInitialStories();
};

//modal actions
const loadInitialStories = () => {
  const currentStorie = Number(sessionStorage.getItem("stories#"));
  readStories(currentStorie + 1);
  renderStoriesModal(currentStorie);
};

const storiesGoNext = () => {
  let currentStorie = Number(sessionStorage.getItem("stories#"));
  currentStorie++;
  sessionStorage.setItem("stories#", currentStorie);
  renderStoriesModal(currentStorie);
};

const storiesGoPrevious = () => {
  let storieIndex = Number(sessionStorage.getItem("stories#"));
  if (!storieIndex) {
    storieIndex = 0;
  } else {
    document.getElementById(`progressBarDiv${storieIndex - 1}`).style.width =
      "100%";
    storieIndex--;
  }
  sessionStorage.setItem("stories#", storieIndex);
  renderStoriesModal(storieIndex);
};

const renderStoriesModal = storieIndex => {
  const slides = document.getElementsByClassName("storiesSlides");
  if (storieIndex > slides.length - 1) {
    shoModalStories(false);
    sessionStorage.setItem("stories#", 0);
  } else if (storieIndex <= slides.length && modalIsOpen === true) {
    if (storieIndex < 0) {
      sessionStorage.setItem("stories#", storieIndex++);
    }
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    slides[storieIndex].style.display = "block";
    renderAnimationHeaderProgressBar(storieIndex);
  }
};

renderAnimationHeaderProgressBar = storieIndex => {
  const progressBar = document.getElementById(`progressBarDiv${storieIndex}`);
  let progressBarWidth = 0;
  let timeStorie = setInterval(() => {
    if (progressBarWidth < 100) {
      if (!isClicked) {
        if (isMoveLeft === true) {
          progressBar.style.width = `100%`;
          clearInterval(timeStorie);
          isMoveLeft = null;
          storiesGoNext();
          pauseVideoStorie(storieIndex);
        } else if (isMoveLeft === false) {
          progressBar.style.width = "0%";
          clearInterval(timeStorie);
          isMoveLeft = null;
          storiesGoPrevious();
        } else {
          progressBarWidth++;
          progressBar.style.width = `${progressBarWidth}%`;
          if (progressBarWidth === 100) {
            storiesGoNext();
            pauseVideoStorie(storieIndex);
            clearInterval(timeStorie);
          }
        }
      }
    }
  }, 10);
};

storiesMouseDown = (event, ctn) => {
  let pWidth = ctn.offsetWidth;
  let x = event.pageX - ctn.offsetLeft;
  mousePressTime = {
    x: x,
    time: new Date().getSeconds(),
    pWidth: pWidth
  };
  isClicked = true;
};

storiesMouseUp = () => {
  if (isClicked && mousePressTime.time === new Date().getSeconds()) {
    if (mousePressTime.pWidth / 2 > mousePressTime.x) {
      isMoveLeft = false;
    } else {
      isMoveLeft = true;
    }
  }
  isClicked = false;
};

// render modal content
const renderHeader = (totalItems, index) => {
  const ctnStoriesHeader = document.getElementById("ctnStoriesHeader");
  const ctnProgressBar = document.createElement("div");
  const progressBar = document.createElement("div");
  ctnProgressBar.setAttribute("class", " ctnProgressBar");
  ctnProgressBar.style.width = `${ctnStoriesHeader.offsetWidth / totalItems -
    10}px`;
  progressBar.setAttribute("id", `progressBarDiv${index}`);
  progressBar.setAttribute("class", " progressBarDiv");
  ctnProgressBar.appendChild(progressBar);
  ctnStoriesHeader.appendChild(ctnProgressBar);
};

renderStorieCtn = (index, asset_url, totalItems, isImage) => {
  const ctnStoriesBody = document.getElementById("ctnStoriesBody");
  const storieNumber = document.createElement("div");
  const ctnImg = document.createElement("div");
  storieNumber.setAttribute("class", "storieNumber");
  storieNumber.appendChild(renderStorieNumberComponent(index, totalItems));
  ctnImg.setAttribute("class", "storiesSlides fade");
  ctnImg.appendChild(storieNumber);
  ctnImg.appendChild(
    isImage
      ? renderImgComponent(asset_url)
      : renderIframeComponent(asset_url, index)
  );
  ctnStoriesBody.appendChild(ctnImg);
};

renderIframeComponent = (asset_url, index) => {
  var iframe = document.createElement("iframe");
  iframe.setAttribute(
    "src",
    `http://www.youtube.com/embed/${GetYouTubeId(
      asset_url
    )}?enablejsapi=1&version=3&playerapiid=ytplayer`
  );
  iframe.width = "99%";
  iframe.height = "480px";
  iframe.setAttribute("id", `storieVideo${index}`);
  return iframe;
};

renderImgComponent = asset_url => {
  const img = document.createElement("img");
  img.setAttribute("src", asset_url);
  return img;
};

renderStorieNumberComponent = (index, totalItems) => {
  return document.createTextNode(`${index + 1} | ${totalItems}`);
};

pauseVideoStorie = index => {
  if ($(`#storieVideo${index}`)[0]) {
    $(`#storieVideo${index}`)[0].contentWindow.postMessage(
      '{"event":"command","func":"pauseVideo","args":""}',
      "*"
    );
  }
};

GetYouTubeId = url => {
  let id = "";
  url = url
    .replace(/(>|<)/gi, "")
    .split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
  if (url[2] !== undefined) {
    id = url[2].split(/[^0-9a-z_\-]/i);
    id = id[0];
  }
  return id;
};
